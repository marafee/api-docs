# API Documentation #

Contains most API's request and response documented.

### Documenation on Red Dot Payment API ###

### How do I get set up? ###

* Clone this repo in your web server directory
* Just run in your local brower

### How do I update the docs? ###

* Get to know Swagger
* https://swagger.io/swagger-ui/
* Add/Edit/Delete any api in  `docs.json` 
* Run your local browser to reflect changes.


### Need help, ping Rafee ###

* rafee@reddotpay.com
* https://swagger.io/swagger-ui/
